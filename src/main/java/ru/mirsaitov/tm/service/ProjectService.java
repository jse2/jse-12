package ru.mirsaitov.tm.service;

import ru.mirsaitov.tm.entity.Project;
import ru.mirsaitov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name, final Long userId) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.create(name, userId);
    }

    public Project create(final String name, final String description, final Long userId) {
        if (description == null || description.isEmpty()) {
            return null;
        }
        Project project = create(name, userId);
        if (project != null) {
            project.setDescription(description);
        }
        return project;
    }

    public void clear(final Long userId) {
        projectRepository.clear(userId);
    }

    public Project findByIndex(final int index, final Long userId) {
        if (index < 0 || index >= projectRepository.getSize(userId)) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.findByIndex(index, userId);
    }

    public Project findByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.findByName(name, userId);
    }

    public Project findById(final Long id, final Long userId) {
        if (id == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.findById(id, userId);
    }

    public Project removeByIndex(final int index, final Long userId) {
        if (index < 0 || index >= projectRepository.getSize(userId)) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.removeByIndex(index, userId);
    }

    public Project removeByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.removeByName(name, userId);
    }

    public Project removeById(final Long id, final Long userId) {
        if (id == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.removeById(id, userId);
    }

    public Project updateByIndex(final int index, final String name, final Long userId) {
        if (index < 0 || index >= projectRepository.getSize(userId)) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.updateByIndex(index, name, userId);
    }

    public Project updateByIndex(final int index, final String name, final String description, final Long userId) {
        if (description == null || description.isEmpty()) {
            return null;
        }
        Project project = updateByIndex(index, name, userId);
        if (project != null) {
            project.setDescription(description);
        }
        return project;
    }

    public Project updateById(final Long id, final String name, final Long userId) {
        if (id == null) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.updateById(id, name, userId);
    }

    public Project updateById(final Long id, final String name, final String description, final Long userId) {
        if (description == null || description.isEmpty()) {
            return null;
        }
        Project project = updateById(id, name, userId);
        if (project != null) {
            project.setDescription(description);
        }
        return project;
    }

    public List<Project> findAll(final Long userId) {
        if (userId == null) {
            return new ArrayList<>();
        }
        return projectRepository.findAll(userId);
    }

}
