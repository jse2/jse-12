package ru.mirsaitov.tm.service;

import org.junit.Test;
import ru.mirsaitov.tm.entity.Task;
import ru.mirsaitov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TaskServiceTest {

    private final long userId = 1l;

    private final String name = "name";

    private final String description = "description";

    private final TaskService taskService = new TaskService(new TaskRepository());

    @Test
    public void shouldCreate() {
        Task task = taskService.create(name, userId);
        assertEquals(name, task.getName());
        assertEquals(name, task.getDescription());

        task = taskService.create(name, description, userId);
        assertEquals(name, task.getName());
        assertEquals(description, task.getDescription());
    }

    @Test
    public void shoudlClear() {
        Task project = taskService.create(name, userId);
        assertTrue(!taskService.findAll(userId).isEmpty());

        taskService.clear(userId);
        assertTrue(taskService.findAll(userId).isEmpty());
    }

    @Test
    public void shouldFindAll() {
        List<Task> projects = new ArrayList<>();

        projects.add(taskService.create(name, userId));
        assertTrue(projects.equals(taskService.findAll(userId)));

        projects.add(taskService.create(name, description, userId));
        assertTrue(projects.equals(taskService.findAll(userId)));
    }

    @Test
    public void shouldFind() {
        Task task = taskService.create(name, description, userId);

        assertEquals(taskService.findById(task.getId(), userId), task);
        assertEquals(taskService.findByName(task.getName(), userId), task);
        assertEquals(taskService.findByIndex(0, userId), task);

        assertEquals(taskService.findById(1L, userId), null);
        assertEquals(taskService.findByName("1", userId), null);
        assertEquals(taskService.findByIndex(1, userId), null);
    }

    @Test
    public void shouldRemove() {
        Task task = taskService.create(name, description, userId);
        assertEquals(taskService.removeById(task.getId(), userId), task);

        task = taskService.create(name, description, userId);
        assertEquals(taskService.removeByName(task.getName(), userId), task);

        task = taskService.create(name, description, userId);
        assertEquals(taskService.removeByIndex(0, userId), task);

        assertEquals(taskService.removeById(1L, userId), null);
        assertEquals(taskService.removeByName("1", userId), null);
        assertEquals(taskService.removeByIndex(1, userId), null);
    }

    @Test
    public void shouldUpdate() {
        final String updateName = "9";
        final String updateDescription = "10";
        Task task = taskService.create(name, description, userId);

        assertEquals(taskService.updateByIndex(0, updateName, userId), task);
        assertEquals(task.getName(), updateName);

        assertEquals(taskService.updateById(task.getId(), updateDescription, updateName, userId), task);
        assertEquals(task.getName(), updateDescription);
        assertEquals(task.getDescription(), updateName);
    }

    @Test
    public void shouldFindTaskByProjectId() {
        Long projectId = 1L;
        Task task = taskService.create(name, description, userId);
        task.setProjectId(projectId);

        assertEquals(taskService.findTasksByProjectId(projectId, userId).get(0), task);
        assertTrue(taskService.findTasksByProjectId(2L, userId).isEmpty());
    }

    @Test
    public void shouldRemoveFromProject() {
        Long projectId = 1L;
        Task task = taskService.create(name, description, userId);

        assertEquals(task.getProjectId(), null);
        task.setProjectId(projectId);
        assertEquals(task.getProjectId(), projectId);
        taskService.removeTaskFromProject(task.getId(), userId);
        assertEquals(task.getProjectId(), null);
    }

    @Test
    public void shouldSeparationData() {
        final Long anotherUser = 2l;
        Task task = taskService.create(name, description, userId);

        assertEquals(taskService.findByIndex(0, anotherUser), null);
        assertEquals(taskService.findAll(anotherUser).size(), 0);

        taskService.clear(anotherUser);

        assertEquals(taskService.findByIndex(0, userId), task);
        assertEquals(taskService.findAll(userId).size(), 1);
    }

}